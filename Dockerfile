FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:2.10

ARG CI_COMMIT_SHORT_SHA
ARG BUILD_DATE
ARG USER=ansible
ARG DEPLOY_USER
ARG DEPLOY_PASSWORD

LABEL org.opencontainers.image.source="https://gitlab.com/OrangeLabsNetworks/oronap-tooling-deployment" \
    org.opencontainers.image.created=$BUILD_DATE \
    org.opencontainers.image.ref.name="oronap-tooling-deployment" \
    org.opencontainers.image.authors="Morgan Richomme <morgan.richomme@orange.com>" \
    org.opencontainers.image.revision=$CI_COMMIT_SHORT_SHA

ENV HOME /home/$USER
ENV COLLECTION_PATH $HOME/.ansible/collections

# Push basic requirements
COPY requirements.yml /tmp/requirements.yml
WORKDIR $HOME/oronap_tooling

COPY inventory ./inventory/
COPY playbooks ./playbooks/
COPY vars ./vars/
COPY .ssh $HOME/.ssh/
COPY ansible.cfg /etc/ansible/

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# install galaxy deps
RUN AUTH_HEADER=$(printf "%s:%s" $DEPLOY_USER $DEPLOY_PASSWORD | base64) && \
    export AUTH_HEADER && \
    git config \
      --global http.https://gitlab.com/OrangeLabsNetworks/.extraheader \
      "Authorization: Basic $AUTH_HEADER" && \
    mkdir -p "$COLLECTION_PATH" && \
    ansible-galaxy collection install -r /tmp/requirements.yml \
      -p "$COLLECTION_PATH" &&\
    # ADD the user
    chown $USER:$USER -R $HOME && chmod go-rwx -R $HOME

USER $USER
